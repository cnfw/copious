#!/usr/bin/env node

const commander = require('commander');
const fs = require('fs');

const program = new commander.Command();
program.version('0.0.1');

program
  .option('-c, --config <path>', 'Path to a copious json file')
  .option('-s, --suite <path>', 'Path to a bootstrapping file defining the seeding suite.');

program.parse(process.argv);

if (!program.config){
  // See if we can find a copious.json in the working directory
  const defaultPath = process.cwd() + '/copious.json';

  if (fs.existsSync(defaultPath)) {
    program.config = defaultPath;
  }
}

let config: {[index: string]: any} = {};

if (program.config){
  config = require(program.config);
} else {
  config.suite = program.suite;
}

if (!config.suite){
  console.log('No suite file was passed either by command line switch or in the config json');
  process.exit(255);
}

const promise = new Promise((resolve, reject) => {
  /// TODO: This needs to detect relative paths versus absolute paths...
  const suite = require(process.cwd() + '/' + config.suite).default;

  const suitePromise = suite.run();

  suitePromise.then(() => {
    resolve();
  });
});

promise.then(() => {
  process.exit(0);
});
