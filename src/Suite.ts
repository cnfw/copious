import {IScenario} from "./IScenario";
import {Factory} from "./Factory";

export class Suite {
  scenarios: IScenario[] = [];

  public addScenario(scope: string, name: string, seeder: (describe: (message: string) => void, faker: Faker.FakerStatic) => void ) {
    this.scenarios.push({
      name: name,
      execute: seeder
    } as IScenario)
  }

  private static scenarioCount = 0;

  public addSuite(suite: Suite){
    this.scenarios.push(...suite.scenarios);
  }

  public async run() {
    for (const scenario of this.scenarios){

      Suite.scenarioCount++;

      console.log('\x1b[1m' + Suite.scenarioCount + '. \x1b[4m' + scenario.name);
      console.log('\x1b[0m');

      const faker = require('faker');
      const crc32 = require('crc-32');

      faker.seed(crc32.str(scenario.name));

      Factory.setFaker(faker);

      await scenario.execute((message) => {
        console.log('   ' + message);
      }, faker);
    }
  }
}
