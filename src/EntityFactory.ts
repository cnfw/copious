import {Factory} from "./Factory";
import {Connection, Repository} from "typeorm";

export abstract class EntityFactory<T> extends Factory {

  private static connection;

  public static setConnection(connection: Connection) {
    EntityFactory.connection = connection;
  }

  public abstract getEntity();

  public getRepository(): Repository<T> {
    return EntityFactory.connection.getRepository(this.getEntity());
  }

  public async findOrCreateEntity(keyValuePairs: {[index: string]: any}): Promise<T> {
    const constructor = this.getEntity();
    let existingItem = new constructor();
    const repository = this.getRepository();

    try {
      existingItem = await repository.findOne(keyValuePairs);
    } catch (err) {
      for (const [key, value] of Object.entries(keyValuePairs)) {
        existingItem[key] = value;
      }
    }

    await repository.save(existingItem);

    return existingItem;
  }
}